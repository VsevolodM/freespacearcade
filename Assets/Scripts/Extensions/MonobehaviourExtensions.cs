﻿using UnityEngine;

public static class MonobehaviourExtensions
{
    public static void SafeStopCoroutine(this MonoBehaviour monoBehaviour, Coroutine coroutine)
    {
        if (coroutine != null)
        {
            monoBehaviour.StopCoroutine(coroutine);
        }
    }
}
