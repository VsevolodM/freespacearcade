﻿using UnityEngine;

public static class GameObjectExtensions
{
    public static void RemoveCloneFromName(this GameObject gameObject)
    {
        gameObject.name = gameObject.name.Replace("(Clone)", string.Empty);
    }
}
