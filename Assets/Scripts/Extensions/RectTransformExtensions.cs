﻿using UnityEngine;

public static class RectTransformExtensions
{
    public static void SetPivotAndAnchors(this RectTransform trans, Vector2 pivot, Vector2 anchorMin, Vector2 anchorMax)
    {
        trans.pivot = pivot;
        trans.anchorMin = anchorMin;
        trans.anchorMax = anchorMax;
    }

    public static float GetWidth(this RectTransform trans)
    {
        return trans.rect.width;
    }

    public static float GetHeight(this RectTransform trans)
    {
        return trans.rect.height;
    }
}