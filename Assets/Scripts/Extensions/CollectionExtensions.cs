﻿using System.Collections.Generic;
using System.Linq;

public static class CollectionExtensions
{
    public static string ToStringWithSeparator<T>(this IEnumerable<T> collection, char separator = ' ')
    {
        if (collection == null || collection.Count() < 0) return string.Empty;

        string result = string.Empty;
        T[] array = collection.ToArray();

        for (int i = 0; i < array.Length; i++)
        {
            result += array[i].ToString();

            if (i < array.Length - 1)
            {
                result += separator;
            }
        }

        return result;
    }
}
