﻿using System;


    public static class DelegateExtentions
    {
        public static void SafeInvoke(this Action action)
        {
            if (action != null)
            {
                action();
            }
        }

        public static void SafeInvoke<T>(this Action<T> action, T param)
        {
            if (action != null)
            {
                action(param);
            }
        }

        public static void SafeInvoke<T1, T2>(this Action<T1, T2> action, T1 param1, T2 param2)
        {
            if (action != null)
            {
                action(param1, param2);
            }
        }

        public static void SafeInvoke<T1, T2, T3>(this Action<T1, T2, T3> action, T1 param1, T2 param2, T3 param3)
        {
            if (action != null)
            {
                action(param1, param2, param3);
            }
        }

        public static TResult SafeInvoke<TResult>(this Func<TResult> func)
        {
            if (func != null)
            {
                return func();
            }

            return default(TResult);
        }

        public static TResult SafeInvoke<T, TResult>(this Func<T, TResult> func, T param)
        {
            if (func != null)
            {
                return func(param);
            }

            return default(TResult);
        }

        public static TResult SafeInvoke<T1, T2, TResult>(this Func<T1, T2, TResult> func, T1 param1, T2 param2)
        {
            if (func != null)
            {
                return func(param1, param2);
            }

            return default(TResult);
        }

        public static TResult SafeInvoke<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> func, T1 param1, T2 param2, T3 param3)
        {
            if (func != null)
            {
                return func(param1, param2, param3);
            }

            return default(TResult);
        }
    }

