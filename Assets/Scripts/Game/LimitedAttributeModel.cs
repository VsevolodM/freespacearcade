﻿using Newtonsoft.Json;
using System;
using UnityEngine;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class LimitedAttributeModel : AttributeModel
{
    [JsonProperty] public virtual float MaxValue { get; private set; }
    public bool IsFull => Mathf.Approximately(CurrentValue, MaxValue);
    public float Percentage => CurrentValue / MaxValue;

    public void Restore()
    {
        SetCurrentValue(MaxValue);
    }

    public override void SetCurrentValue(float value)
    {
        var newValue = Mathf.Clamp(value, 0, MaxValue);
        base.SetCurrentValue(newValue);
    }
}
