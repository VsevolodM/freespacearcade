﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraFollow : BaseDataContextView<Transform>
{
    [SerializeField] private float smoothSpeed = 0.065f;
    [SerializeField] private Vector3 offset = new Vector3(0f, 0f, 0f);

    private void FixedUpdate()
    {
        if (ContextData != null)
        {
            Vector3 desiredPosition = ContextData.position + offset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = smoothedPosition;
        }
    }
}