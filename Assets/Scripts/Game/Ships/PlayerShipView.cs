﻿using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerShipView : BaseShipView
{
    private Camera mainCamera;

    protected override void Awake()
    {
        base.Awake();
        mainCamera = Camera.main;
    }

    private void FixedUpdate()
    {
        if (!EventSystem.current.IsPointerOverGameObject() && !ContextData.IsDestroyed)
        {
            Vector3 mouse = Input.mousePosition;
            Vector3 mouseWorld = mainCamera.ScreenToWorldPoint(new Vector3(mouse.x, mouse.y, transform.position.z));
            transform.up = mouseWorld - transform.position;

            var horizontal = Input.GetAxis("Horizontal");
            var vertical = Input.GetAxis("Vertical");
            var offset = new Vector2(horizontal, vertical) * speed;

            rigidbody2d.MovePosition(rigidbody2d.position + offset);

            if (Input.GetMouseButtonUp(0))
            {
                projectileGunView.Shoot();
            }

            if (Input.GetMouseButtonUp(1))
            {
                if (ContextData.RocketGun.UpgradableAmmoModel.CurrentValue > 0)
                {
                    rocketGunView.Shoot();
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collider2D)
    {
        var coinView = collider2D.GetComponent<CoinView>();

        if (coinView != null)
        {
            coinView.ReturnToPool();
            GlobalEventsListener.Call(GlobalEvent.CoinPicked);
        }
    }
}
