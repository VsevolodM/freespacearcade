﻿using DG.Tweening;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyShipView : BaseShipView, IPoolable
{
    private EnemyShipModel enemyShipModel;
    private Vector2 targetPosition;
    private BaseShipView targetEnemy;
    private WaitForSeconds shootInterval;
    private Coroutine scanForShipsCoroutine;
    private Coroutine movingCoroutine;
    private Coroutine shootingCoroutine;
   
    protected override void OnInitialize()
    {
        base.OnInitialize();
        enemyShipModel = (EnemyShipModel) ContextData;
        shootInterval = new WaitForSeconds(enemyShipModel.ShootInterval);
        targetPosition = transform.position;
        ContextData.OnDestroyedEvent += OnDestroyed;
        StartSimulation();
    }

    private void StartSimulation()
    {
        void OnEnemySpotted()
        {
            if (movingCoroutine != null)
            {
                StopCoroutine(movingCoroutine);
            }

            shootingCoroutine = StartCoroutine(ShootWhileEnemyIsAlive(OnEnemyLost));
        }

        void OnEnemyLost()
        {
            if (shootingCoroutine != null)
            {
                StopCoroutine(shootingCoroutine);
            }

            Simulate();
        }

        void Simulate()
        {
            scanForShipsCoroutine = StartCoroutine(ScanForShips(OnEnemySpotted));
            movingCoroutine = StartCoroutine(Moving());
        }

        Simulate();
    }

    private IEnumerator Moving()
    {
        while (true)
        {
            ChooseRandomTargetPosition();
            yield return RotateToPoint(targetPosition);
            yield return MoveToTargetPoint();
        }
    }

    private IEnumerator ScanForShips(Action callback)
    {
        while (targetEnemy == null)
        {
            targetEnemy = GetEnemyInScanRadius();
            yield return Constants.SHIPS_SCAN_INTERVAL;
        }

        callback?.Invoke();
    }

    private BaseShipView GetEnemyInScanRadius()
    {
        Collider2D[] overlapResults = new Collider2D[App.AppModel.RoundsManager.Enemies.Count + 1];
        int collidersAmount = Physics2D.OverlapCircleNonAlloc(transform.position, enemyShipModel.ShipScanRadius, overlapResults, Constants.SHIPS_LAYER_MASK);

        if (collidersAmount > 0)
        {
            var ships = overlapResults.Where(col => col != null)
                .Select(col => col.gameObject.GetComponent<BaseShipView>())
                .Where(sh => sh != null && sh.ContextData.Id != ContextData.Id && !sh.ContextData.IsDestroyed).ToList();
            
            if (ships.Any())
            {
                return ships.ElementAt(0);
            }
        }

        return null;
    }

    private IEnumerator RotateToPoint(Vector2 point)
    {
        var dir = point - (Vector2) transform.position;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90f;
        var rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        var rotationTween = transform.DORotate(rotation.eulerAngles, enemyShipModel.RotationSpeed).SetEase(Ease.Linear).SetSpeedBased();
        yield return rotationTween.WaitForCompletion();
    }

    private IEnumerator MoveToTargetPoint()
    {
        var movingTween = transform.DOMove(targetPosition, enemyShipModel.MoveSpeed).SetEase(Ease.Linear).SetSpeedBased();
        yield return movingTween.WaitForCompletion();
    }

    private IEnumerator ShootWhileEnemyIsAlive(Action callback)
    {
        while (targetEnemy != null && !targetEnemy.ContextData.IsDestroyed && IsInShootingRange(targetEnemy))
        {
            yield return RotateToPoint(targetEnemy.transform.position);
            bool shootWithRocket = Random.value <= enemyShipModel.RocketShotProbability;

            if (shootWithRocket && enemyShipModel.RocketGun.UpgradableAmmoModel.CurrentValue > 0)
            {
                rocketGunView.Shoot(); 
            }
            else
            {
                projectileGunView.Shoot();
            }

            yield return shootInterval;
        }

        targetEnemy = null;
        callback?.Invoke();
    }

    private float GetDistanceToShip(BaseShipView view)
    {
        var enemyClosestPoint = view.Collider2D.ClosestPoint(transform.position);
        var shipClosesPoint = Collider2D.ClosestPoint(enemyClosestPoint);
        var distance = Vector2.Distance(enemyClosestPoint, shipClosesPoint);
        return distance;
    }

    private bool IsInShootingRange(BaseShipView view)
    {
        return GetDistanceToShip(view) <= enemyShipModel.ShipScanRadius;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Constants.GIZMOS_TARGET_POSITION_SPHERE_COLOR;
        Gizmos.DrawSphere(targetPosition, Constants.GIZMOS_TARGET_POSITION_SPHERE_RADIUS);
        Gizmos.DrawLine(transform.position, targetPosition);
        Gizmos.color = Constants.GIZMOS_SHIP_SCAN_RADIUS_COLOR;
        Gizmos.DrawSphere((Vector2)transform.position, enemyShipModel.ShipScanRadius);
    }

    private void ChooseRandomTargetPosition()
    {
        var randomOffset = Random.insideUnitCircle * enemyShipModel.RoamingRadiusMultiplier;
        targetPosition = (Vector2)transform.position + randomOffset;
    }

    private void OnDestroyed()
    {
        var coin = App.AppModel.CoinsPool.GetItem(transform.position);
        coin.Initialize(App.AppModel.AppSettings.GeneralGameSettings.CoinLifespanSec);
        ReturnToPool();
    }

    private void OnDisable()
    {
        if (ContextData != null)
        {
            ContextData.OnDestroyedEvent -= OnDestroyed;
        }
    }

    public void ReturnToPool()
    {
        App.AppModel.EnemyShipsPool.ReturnItem(this);
    }
}
