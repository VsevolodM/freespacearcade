﻿using System;
using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class EnemyShipModel : BaseShipModel
{
    [JsonProperty] public float ShipScanRadius { get; private set; }
    [JsonProperty] public float ShootInterval { get; private set; }
    [JsonProperty] public float RocketShotProbability { get; private set; }
    [JsonProperty] public float MoveSpeed { get; private set; }
    [JsonProperty] public float RotationSpeed { get; private set; }
    [JsonProperty] public float RoamingRadiusMultiplier { get; private set; }

    public override object Clone()
    {
        return this.DeepClone();
    }

    public override string ToString()
    {
        return $"{base.ToString()}, ShipScanRadius: {ShipScanRadius}, ShotInterval: {ShootInterval}, RocketShotProbability: {RocketShotProbability}, MoveSpeed: {MoveSpeed}, RotationSpeed: {RotationSpeed}, RoamingRadiusMultiplier: {RoamingRadiusMultiplier}";
    }
}
