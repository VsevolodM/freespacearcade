﻿using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(Rigidbody2D))]
public abstract class BaseShipView : BaseDataContextView<BaseShipModel>
{
    [SerializeField] protected float speed = 1f;
    [SerializeField] protected GunView projectileGunView;
    [SerializeField] protected GunView rocketGunView;

    public Collider2D Collider2D { get; private set; }

    protected Rigidbody2D rigidbody2d;

    protected virtual void Awake()
    {
        Assert.IsNotNull(projectileGunView);
        Assert.IsNotNull(rocketGunView);

        rigidbody2d = GetComponent<Rigidbody2D>();
        Collider2D = GetComponent<Collider2D>();
    }

    protected override void OnInitialize()
    {
        base.OnInitialize();

        projectileGunView.Initialize(ContextData.ProjectileGun);
        rocketGunView.Initialize(ContextData.RocketGun);

        gameObject.name = string.Format("{0}[{1}]", gameObject.name, ContextData.Id);
    }
}
