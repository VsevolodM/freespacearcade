﻿using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class BaseShipModel : ICloneable
{
    public event Action OnDestroyedEvent;

    public int Id { get; private set; }

    [JsonProperty] public LimitedAttributeModel LivesModel { get; private set; }
    [JsonProperty] public LimitedAttributeModel HealthModel { get; private set; }
    [JsonProperty] public RechargableAttributeModel ShieldsModel { get; private set; }
    [JsonProperty] public GunModel ProjectileGun { get; private set; }
    [JsonProperty] public LimitedGunModel RocketGun { get; private set; }

    private int lastDamagerId;
    public bool IsDestroyed { get; private set; }

    [OnDeserialized]
    private void OnDeserializedMethod(StreamingContext context)
    {
        HealthModel.OnChangedEvent += OnHealthChanged;
    }

    public void SetId(int id)
    {
        Id = id;
        ProjectileGun.Initialize(id, App.AppModel.ProjectilePool);
        RocketGun.Initialize(id, App.AppModel.RocketsPool);
    }

    ~BaseShipModel()
    {
        HealthModel.OnChangedEvent -= OnHealthChanged;
    }

    public void ApplyDamage(float damage, int damagerId)
    {
        lastDamagerId = damagerId;

        float damageLeft = ShieldsModel.CapacityAttributeModel.CurrentValue - damage;
        ShieldsModel.CapacityAttributeModel.Subtract(damage);

        if (damageLeft < 0)
        {
            HealthModel.Subtract(-damageLeft);
        }
    }

    private void OnHealthChanged()
    {
        if (HealthModel.CurrentValue <= 0 && !IsDestroyed)
        {
            LivesModel.Subtract(1);

            if (LivesModel.CurrentValue <= 0)
            {
                SelfDestroy();
            }
            else
            {
                HealthModel.Restore();
                ShieldsModel.CapacityAttributeModel.Restore();
            }
        }
    }

    private void SelfDestroy()
    {
        IsDestroyed = true;
        GlobalEventsListener.Call(GlobalEvent.EnemyDestroyed, this, lastDamagerId);
        OnDestroyedEvent?.Invoke();
    }

    public virtual object Clone()
    {
        return this.DeepClone();
    }

    public override string ToString()
    {
        return $"Id: {Id}, LivesModel: {LivesModel}, HealthModel: {HealthModel}, ShieldsModel: {ShieldsModel}";
    }
}
