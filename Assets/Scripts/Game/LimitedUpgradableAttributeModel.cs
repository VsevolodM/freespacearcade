﻿using Newtonsoft.Json;
using System;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class LimitedUpgradableAttributeModel : LimitedAttributeModel
{
    public override float MaxValue => Upgradable.GetValue();
    [JsonProperty] public UpgradableModel Upgradable { get; private set; }
}
