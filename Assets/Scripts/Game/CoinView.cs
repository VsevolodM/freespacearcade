﻿using UnityEngine;

public class CoinView : BaseDataContextView<float>, IPoolable
{
    private Coroutine dissapearCoroutine;

    protected override void OnInitialize()
    {
        base.OnInitialize();
        dissapearCoroutine = CoroutinesHelper.CoroutinesHolder.ExecuteWithDelay(ContextData, ReturnToPool);
    }

    private void OnDisable()
    {
        CoroutinesHelper.CoroutinesHolder.SafeStopCoroutine(dissapearCoroutine);
    }

    public void ReturnToPool()
    {
        App.AppModel.CoinsPool.ReturnItem(this);
    }
}
