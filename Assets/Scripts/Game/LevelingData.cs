﻿using System;
using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class LevelingData
{
    [JsonProperty] public uint Level { get; private set; }
    [JsonProperty] public float Value { get; private set; }

    public override string ToString()
    {
        return $"Level {Level}, Value: {Value}";
    }
}