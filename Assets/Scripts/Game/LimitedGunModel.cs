﻿using Newtonsoft.Json;
using System;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class LimitedGunModel : GunModel
{
    [JsonProperty] public LimitedUpgradableAttributeModel UpgradableAmmoModel { get; private set; }

    protected override void OnShot()
    {
        base.OnShot();
        UpgradableAmmoModel.Subtract(1);
    }
}
