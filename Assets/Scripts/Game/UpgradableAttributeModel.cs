﻿using Newtonsoft.Json;
using System;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class UpgradableAttributeModel : AttributeModel
{
    public override float CurrentValue => Upgradable.GetValue();
    [JsonProperty] public UpgradableModel Upgradable { get; private set; }
}
