﻿using UnityEngine;

public class GunView : BaseDataContextView<GunModel>
{
    [SerializeField] private Transform projectileSpawnpoint;

    public void Shoot()
    {
        ContextData.Shoot(projectileSpawnpoint);
    }
}
