﻿using Newtonsoft.Json;
using System;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class LevelingDataManager
{
    [JsonProperty] public LevelingData[] Levels { get; private set; }

    public float GetValueByLevel(uint level)
    {
        var levelingData = GetLevelingDataByLevel(level);

        if (levelingData != null)
        {
            return levelingData.Value;
        }

        return 0f;
    }

    public LevelingData GetLevelingDataByLevel(uint level)
    {
        for (int i = 0; i < Levels.Length; i++)
        {
            if (Levels[i].Level == level)
            {
                return Levels[i];
            }
        }

        return null;
    }

    public uint GetMaxPossibleLevel()
    {
        uint maxLevel = 0;

        for (int i = 0; i < Levels.Length; i++)
        {
            uint currentLevel = Levels[i].Level;

            if (currentLevel > maxLevel)
            {
                maxLevel = currentLevel;
            }
        }

        return maxLevel;
    }

    public override string ToString()
    {
        return $"Levels: {Levels.ToStringWithSeparator()}";
    }
}