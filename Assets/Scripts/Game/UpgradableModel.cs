﻿using System;
using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class UpgradableModel
{
    public event Action OnLevelChangedEvent;

    [JsonProperty] public uint Level { get; private set; }

    public bool IsFullyUpgraded => Level == manager.GetMaxPossibleLevel();

    private LevelingDataManager manager;

    public void SetManager(LevelingDataManager manager)
    {
        this.manager = manager;
    }

    public float GetValue()
    {
        return manager.GetValueByLevel(Level);
    }

    public void Upgrade()
    {
        uint maxPossibleLevel = manager.GetMaxPossibleLevel();

        if (Level < maxPossibleLevel)
        {
            SetLevel(Level + 1);
        }
    }

    private void SetLevel(uint level)
    {
        Level = level;
        OnLevelChangedEvent?.Invoke();
    }
}
