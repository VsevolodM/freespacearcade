﻿using System;
using Newtonsoft.Json;
using UnityEngine;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class GunModel
{
    [JsonIgnore] public int OwnerId { get; private set; }
    [JsonProperty] public ProjectileModel ProjectileModel { get; private set; }

    [JsonIgnore] public Pool<ProjectileView> Pool { get; private set; }

    public void Initialize(int ownerId, Pool<ProjectileView> pool)
    {
        OwnerId = ownerId;
        Pool = pool;
    }

    public void Shoot(Transform spawnPoint)
    {
        var model = (ProjectileModel)ProjectileModel.Clone();
        var view = Pool.GetItem(spawnPoint.transform.position);
        view.transform.rotation = spawnPoint.transform.rotation;
        model.Initialize(OwnerId, Pool);
        view.Initialize(model);
        OnShot();
    }

    protected virtual void OnShot() { }

    public override string ToString()
    {
        return $"OwnerId: {OwnerId}, ProjectileModel: {ProjectileModel}, Pool: {Pool}";
    }
}
