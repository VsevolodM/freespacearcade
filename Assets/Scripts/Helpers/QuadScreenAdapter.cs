﻿using UnityEngine;

[ExecuteInEditMode]
public class QuadScreenAdapter : MonoBehaviour
{
    private Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    private void Update()
    {
        float width = mainCamera.orthographicSize * 2.0f * Screen.width / Screen.height;
        float height = mainCamera.orthographicSize * 2.0f;

        float scale = Mathf.Max(width, height);

        transform.localScale = new Vector3(scale, scale, 0.1f);
    }
}