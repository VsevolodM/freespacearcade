﻿using DG.Tweening;
using System;
using UnityEngine;

public class MoveDialogAnimator : BaseDialogAnimator
{
    [SerializeField] private float moveDuration = 0.5f;
    [SerializeField] private PositionSettings appearPosition;
    [SerializeField] private PositionSettings idlePosition;
    [SerializeField] private PositionSettings disappearPosition;

    public override void PlayShowAnimation(Action callback = null)
    {
        body.SetPivotAndAnchors(appearPosition.Pivot, appearPosition.AnchorMin, appearPosition.AnchorMax);
        body.anchoredPosition = appearPosition.AnchoredPosition;
        PlayAnimation(idlePosition, callback);
    }

    public override void PlayHideAnimation(Action callback = null)
    {
        PlayAnimation(disappearPosition, callback);
    }

    public override void PlayHideAnimationInstant(Action callback = null)
    {
        PlayAnimationInstant(disappearPosition, callback);
    }

    public override void PlayShowAnimationInstant(Action callback = null)
    {
        PlayAnimationInstant(idlePosition, callback);
    }

    private void PlayAnimation(PositionSettings targetSettings, Action callback = null)
    {
        body.DOKill();
        body.DOPivot(targetSettings.Pivot, moveDuration).SetUpdate(true);
        body.DOAnchorMin(targetSettings.AnchorMin, moveDuration).SetUpdate(true);
        body.DOAnchorMax(targetSettings.AnchorMax, moveDuration).OnComplete(callback.SafeInvoke).SetUpdate(true);
        body.DOAnchorPos(targetSettings.AnchoredPosition, moveDuration).SetUpdate(true);
    }

    private void PlayAnimationInstant(PositionSettings targetSettings, Action callback = null)
    {
        body.DOKill();
        body.SetPivotAndAnchors(targetSettings.Pivot, targetSettings.AnchorMin, targetSettings.AnchorMax);
        body.anchoredPosition = targetSettings.AnchoredPosition;
    }
}