﻿using System;
using UnityEngine;

[Serializable]
public struct PositionSettings
{
    [SerializeField] private Vector2 anchoredPosition;
    [SerializeField] private Vector2 anchorMin;
    [SerializeField] private Vector2 anchorMax;
    [SerializeField] private Vector2 pivot;

    public Vector2 AnchoredPosition => anchoredPosition;
    public Vector2 AnchorMin => anchorMin;
    public Vector2 AnchorMax => anchorMax;
    public Vector2 Pivot => pivot;
}