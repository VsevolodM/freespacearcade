﻿using System;
using System.Collections.Generic;

public static class GlobalEventsListener
{
    private static readonly Dictionary<GlobalEvent, List<GlobalAction>> events = new Dictionary<GlobalEvent, List<GlobalAction>>();

    private static int eventId;

    public static int Register(GlobalEvent globalEvent, Action action)
    {
        GlobalAction globalAction = new GlobalAction(++eventId, action);
        Register(globalEvent, globalAction);
        return eventId;
    }

    public static int Register<T>(GlobalEvent globalEvent, Action<T> action)
    {
        GlobalAction globalAction = new GlobalAction(++eventId, action);
        Register(globalEvent, globalAction);
        return eventId;
    }

    public static int Register<T, V>(GlobalEvent globalEvent, Action<T, V> action)
    {
        GlobalAction globalAction = new GlobalAction(++eventId, action);
        Register(globalEvent, globalAction);
        return eventId;
    }

    private static void Register(GlobalEvent globalEvent, GlobalAction deleg)
    {
        if (!events.ContainsKey(globalEvent))
        {
            events.Add(globalEvent, new List<GlobalAction> { deleg });
        }
        else
        {
            events[globalEvent].Add(deleg);
        }
    }

    public static void Unregister(GlobalEvent globalEvent, int id)
    {
        if (events.ContainsKey(globalEvent))
        {
            foreach (var ev in events[globalEvent])
            {
                if (ev.Id == id)
                {
                    events[globalEvent].Remove(ev);
                    break;
                }
            }
        }
    }

    public static void Call(GlobalEvent globalEvent)
    {
        if (events.ContainsKey(globalEvent))
        {
            foreach (var globalAction in events[globalEvent])
            {
                (globalAction.Delegate as Action).SafeInvoke();
            }
        }
    }

    public static void Call<T, V>(GlobalEvent globalEvent, T argument, V argument2)
    {
        if (events.ContainsKey(globalEvent))
        {
            for (int i = events[globalEvent].Count - 1; i >= 0; i--)
            {
                (events[globalEvent][i].Delegate as Action<T, V>).SafeInvoke(argument, argument2);
            }
        }
    }
}

public class GlobalAction
{
    public int Id { get; }
    public Delegate Delegate { get; }

    public GlobalAction(int id, Delegate deleg)
    {
        Id = id;
        Delegate = deleg;
    }
}

public enum GlobalEvent
{
    EnemyDestroyed = 0,
    CoinPicked = 5
}