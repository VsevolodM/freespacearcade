﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;
using Object = System.Object;

public static class AddressableHelper
{
    private static readonly Dictionary<string, Object> cache = new Dictionary<string, Object>();
    private static readonly int cacheSize = 50;

    public static void LoadObject<T>(string addressableId, Action<T> callback) where T : class
    {
        if (!cache.ContainsKey(addressableId))
        {
            var operation = Addressables.LoadAssetAsync<T>(addressableId);

            operation.Completed += OnLoaded;

            void OnLoaded(AsyncOperationHandle<T> result)
            {
                if (!cache.ContainsKey(addressableId))
                {
                    cache.Add(addressableId, null);

                    while (cache.Count > cacheSize)
                    {
                        cache.Remove(cache.ElementAt(0).Key);
                    }
                }

                cache[addressableId] = result.Result;
                callback?.Invoke(result.Result);
            }
        }
        else
        {
            callback?.Invoke((T)cache[addressableId]);
        }
    }

    public static void LoadSprite(string addressableId, Image image, Action callback = null)
    {
        void OnLoaded(Sprite sprite)
        {
            image.sprite = sprite;
            callback?.Invoke();
        }

        LoadObject<Sprite>(addressableId, OnLoaded);
    }

    public static void LoadSprite(string addressableId, SpriteRenderer spriteRenderer)
    {
        LoadObject<Sprite>(addressableId, (sprite) => spriteRenderer.sprite = sprite);
    }
}
