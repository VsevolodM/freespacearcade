﻿using System.Collections.Generic;
using UnityEngine;

public class ViewSpawner : MonoBehaviour
{
    [SerializeField] private Transform targetTransform;
    [SerializeField] private GameObject prefab;

    private readonly List<GameObject> cache = new List<GameObject>();
    private const string VIEWSPAWNER_ENABLED = "ViewSpawnerEnabled";
    private const string VIEWSPAWNER_DISABLED = "ViewSpawnerDisabled";

    private void Awake()
    {
        if(prefab != null)
        { 
            prefab.SetActive(false);
        }
    }

    public V Spawn<T, V>(T model) where V : BaseDataContextView<T>
    {
        GameObject view = GetCachedItem();

        if (view == null)
        {
            view = Instantiate(prefab, targetTransform);
            view.transform.localScale = Vector3.one;
            view.transform.localPosition = Vector3.zero;
            cache.Add(view);
        }

        V contextView = view.GetComponent<V>();
        contextView.gameObject.SetActive(true);
        contextView.gameObject.tag = VIEWSPAWNER_ENABLED;
        contextView.Initialize(model);
        return contextView;
    }

    public List<V> Spawn<T, V>(IList<T> list) where V : BaseDataContextView<T>
    {
        List<V> result = new List<V>();

        for (int i = 0; i < cache.Count; i++)
        {
            cache[i].tag = VIEWSPAWNER_DISABLED;
        }

        for (int i = 0; i < list.Count; i++)
        {
            V item = Spawn<T, V>(list[i]);
            result.Add(item);
        }

        for (int i = list.Count; i < cache.Count; i++)
        {
            cache[i].tag = VIEWSPAWNER_DISABLED;
            cache[i].SetActive(false);
        }

        return result;
    }

    private GameObject GetCachedItem()
    {
        for (int i = 0; i < cache.Count; i++)
        {
            if (cache[i].tag == VIEWSPAWNER_DISABLED)
            {
                return cache[i];
            }
        }

        return null;
    }
}