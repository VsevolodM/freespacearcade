﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

[Serializable]
public class Pool<T> where T : MonoBehaviour, IPoolable
{
    private readonly List<T> items = new List<T>();
    private readonly T itemPrefab;
    private readonly Transform itemsParent;

    public Pool(string parentName, string prefabPath, int initialAmount)
    {
        itemPrefab = Resources.Load<T>(prefabPath);
        itemsParent = new GameObject(parentName).transform;
        Object.DontDestroyOnLoad(itemsParent.gameObject);

        for (int i = 0; i < initialAmount; i++)
        {
            var newItem = SpawnItem();
            items.Add(newItem);
            newItem.gameObject.SetActive(false);
        }
    }

    public T GetItem(Vector2 position)
    {
        T item;

        if (items.Count > 0)
        {
            item = items[0];
            items.RemoveAt(0);
        }
        else
        {
            item = SpawnItem();
        }

        item.transform.position = position;
        item.gameObject.SetActive(true);
        return item;
    }

    public void GetAllItemsBack()
    {
        for (int i = 0; i < itemsParent.childCount; i++)
        {
            var poolableItem = itemsParent.GetChild(i).GetComponent<IPoolable>();

            if (poolableItem is T)
            {
                ReturnItem((T)poolableItem);
            }
        }
    }

    public void ReturnItem(T item)
    {
        items.Add(item);
        item.gameObject.SetActive(false);
    }

    private T SpawnItem()
    {
        var newItem = Object.Instantiate(itemPrefab, itemsParent).GetComponent<T>();
        newItem.gameObject.RemoveCloneFromName();
        newItem.gameObject.SetActive(true);
        return newItem;
    }
}
