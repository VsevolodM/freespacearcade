﻿using UnityEngine;

public static class CoroutinesHelper
{
    public static CoroutinesHolder CoroutinesHolder { get; }

    static CoroutinesHelper()
    {
        var coroutinesHelperGameObject = new GameObject(Constants.COROUTINE_HELPER_NAME, typeof(CoroutinesHolder));
        CoroutinesHolder = coroutinesHelperGameObject.GetComponent<CoroutinesHolder>();
    }
}
