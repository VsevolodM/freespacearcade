﻿using System;
using System.Collections;
using UnityEngine;

public class CoroutinesHolder : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public Coroutine ExecuteWithDelay(float delay, Action action)
    {
        return StartCoroutine(ExecuteWithDelayCoroutine(delay, action));
    }

    private IEnumerator ExecuteWithDelayCoroutine(float delay, Action action)
    {
        yield return new WaitForSeconds(delay);
        action?.Invoke();
    }
}
