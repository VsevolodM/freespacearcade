﻿using UnityEngine;

public class TextureScroller : BaseDataContextView<Transform>
{
    [SerializeField] private float scrollMultiplier = 1f;
    [SerializeField] private MeshRenderer meshRenderer;

    private void Update()
    {
        if (ContextData != null)
        {
            meshRenderer.material.mainTextureOffset = ContextData.position * scrollMultiplier;
        }
    }
}
