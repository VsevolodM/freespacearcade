﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public static class AudioManager 
{
    private const string SOUND_NAME_PATTERN = "AudioSource[{0}]";
    private const string MUSIC_NAME_PATTERN = "MusicAudioSource[{0}]";
    private static Transform audioParent;
    private static AudioSource musicAudioSource;

    private static List<AudioSource> soundAudioSources = new List<AudioSource>();

    static AudioManager()
    {
        audioParent = new GameObject("AudioManager").transform;
        Object.DontDestroyOnLoad(audioParent);
        musicAudioSource = CreateMusicAudioSource();
        CreateAudioListener();
    }

    public static void PlaySound(string addressable, Action<AudioClip> callback = null)
    {
        void OnSoundLoaded(AudioClip clip)
        {
            PlaySound(clip);
            callback?.Invoke(clip);
        }

        AddressableHelper.LoadObject<AudioClip>(addressable, OnSoundLoaded);
    }

    public static void PlayMusic(string addressable, float fadeDuration, Action<AudioSource> callback = null)
    {
        void OnMusicLoaded(AudioClip clip)
        {
            PlayMusic(clip, fadeDuration);
            callback?.Invoke(musicAudioSource);
        }

        AddressableHelper.LoadObject<AudioClip>(addressable, OnMusicLoaded);
    }

    private static void PlaySound(AudioClip clip)
    {
        AudioSource audioSource = GetFreeSoundAudioSource();

        if (clip != null)
        {
            audioSource.gameObject.name = string.Format(SOUND_NAME_PATTERN, clip.name);
            audioSource.clip = clip;
            audioSource.Play();
        }
    }

    private static void PlayMusic(AudioClip clip, float fadeDuration)
    {
        musicAudioSource.DOKill();

        musicAudioSource.gameObject.name = string.Format(MUSIC_NAME_PATTERN, clip.name);
        musicAudioSource.clip = clip;
        musicAudioSource.volume = 0f;

        musicAudioSource.DOFade(1f, fadeDuration);

        musicAudioSource.Play();
    }

    public static void StopMusic(float duration)
    {
        musicAudioSource.DOKill();
        musicAudioSource.DOFade(duration, 0f).OnComplete(() => musicAudioSource.Stop());
    }

    private static AudioSource GetFreeSoundAudioSource()
    {
        for (int i = 0; i < soundAudioSources.Count; i++)
        {
            if (!soundAudioSources[i].isPlaying)
            {
                return soundAudioSources[i];
            }
        }

        return CreateSoundAudioSource();
    }

    private static AudioSource CreateMusicAudioSource()
    {
        AudioSource audioSource = CreateNewAudioSource();
        audioSource.name = "MusicAudioSource";
        audioSource.loop = true;
        audioSource.playOnAwake = true;
        return audioSource;
    }

    private static AudioSource CreateSoundAudioSource()
    {
        AudioSource audioSource = CreateNewAudioSource();
        soundAudioSources.Add(audioSource);
        return audioSource;
    }

    private static AudioSource CreateNewAudioSource()
    {
        Transform newAudioSourceTransform = new GameObject("NewAudioSource", typeof(AudioSource)).transform;
        newAudioSourceTransform.SetParent(audioParent);
        return newAudioSourceTransform.GetComponent<AudioSource>();
    }

    private static AudioListener CreateAudioListener()
    {
        Transform newAudioListenerTransform = new GameObject("AudioListener", typeof(AudioListener)).transform;
        newAudioListenerTransform.SetParent(audioParent);
        return newAudioListenerTransform.GetComponent<AudioListener>();
    }
}

