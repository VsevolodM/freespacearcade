﻿public class ShopModel
{
    public ShopSettings ShopSettings { get; private set; }

    public ShopModel(ShopSettings shopSettings)
    {
        ShopSettings = shopSettings;
    }
}
