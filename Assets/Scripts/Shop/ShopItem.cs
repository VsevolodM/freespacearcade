﻿using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class ShopItem
{
    [JsonProperty] public string DisplayName { get; private set; }
    [JsonProperty] public ShopItemType ShopItemType { get; private set; }
    [JsonProperty] public uint Price { get; private set; }
    [JsonProperty] public uint? AmountPerSession { get; private set; }

    private uint boughtItemsAmount;

    public void SetBoughtItemsAmountToZero()
    {
        boughtItemsAmount = 0;
    }

    public bool HasPlayerEnoughCoinsToBuy()
    {
        return App.AppModel.CoinsAmount >= Price;
    }

    public bool CanBuyInThisShoppingSession()
    {
        if (AmountPerSession == null) return true;

        return boughtItemsAmount < AmountPerSession.Value;
    }

    public void Buy()
    {
        if (!HasPlayerEnoughCoinsToBuy())
        {
            ShowDontHaveEnoughCoinsToBuy();
            return;
        }
        else if (!CanBuyInThisShoppingSession())
        {
            ShowCantBuyMoreThisSessionDialog();
            return;
        }

        switch (ShopItemType)
        {
            case ShopItemType.PlusLife:
                var livesModel = App.AppModel.AppSettings.PlayerModel.LivesModel;
                if (!livesModel.IsFull)
                {
                    livesModel.Add(1);
                }
                else
                {
                    ShowItemIsAlreadyMaxed();
                }

                break;
            case ShopItemType.RechargeRockets:
                var rocketGun = App.AppModel.AppSettings.PlayerModel.RocketGun;

                if (!rocketGun.UpgradableAmmoModel.IsFull)
                {
                    rocketGun.UpgradableAmmoModel.Restore();
                }
                else
                {
                    ShowItemIsAlreadyMaxed();
                }

                break;
            case ShopItemType.UpgradeRocketAmmo:
                var rocketGun2 = App.AppModel.AppSettings.PlayerModel.RocketGun;

                if (!rocketGun2.UpgradableAmmoModel.Upgradable.IsFullyUpgraded)
                {
                    rocketGun2.UpgradableAmmoModel.Upgradable.Upgrade();
                }
                else
                {
                    ShowItemIsAlreadyMaxed();
                }
                
                break;
            case ShopItemType.UpgradeShieldCapacity:
                var shieldsModel = App.AppModel.AppSettings.PlayerModel.ShieldsModel;

                if (!shieldsModel.CapacityAttributeModel.Upgradable.IsFullyUpgraded)
                {
                    shieldsModel.CapacityAttributeModel.Upgradable.Upgrade();
                    shieldsModel.CapacityAttributeModel.Restore();
                }
                else
                {
                    ShowItemIsAlreadyMaxed();
                }

                break;
            case ShopItemType.UpgradeShieldRechargeRate:
                var shieldsModel2 = App.AppModel.AppSettings.PlayerModel.ShieldsModel;

                if (!shieldsModel2.RechargeAttributeModel.Upgradable.IsFullyUpgraded)
                {
                    shieldsModel2.RechargeAttributeModel.Upgradable.Upgrade();
                }
                else
                {
                    ShowItemIsAlreadyMaxed();
                }

                break;
        }

        App.AppModel.SubtractCoinsAmount(Price);
        boughtItemsAmount++;
    }

    private void ShowCantBuyMoreThisSessionDialog()
    {
        var infoDialogData = new InfoDialogData("Oops!", "You can't buy more of this item during this shopping session!");
        App.AppService.NavigationService.OpenInfoDialog(infoDialogData);
    }

    private void ShowDontHaveEnoughCoinsToBuy()
    {
        var infoDialogData = new InfoDialogData("Oops!", "You don't have enough coins to buy this item!");
        App.AppService.NavigationService.OpenInfoDialog(infoDialogData);
    }

    private void ShowItemIsAlreadyMaxed()
    {
        var infoDialogData = new InfoDialogData("Oops!", "You have maxed out the amount of this item!");
        App.AppService.NavigationService.OpenInfoDialog(infoDialogData);
    }

    public override string ToString()
    {
        return $"DisplayName: {DisplayName}, ShopItemType: {ShopItemType}, Price: {Price}";
    }
}

public enum ShopItemType
{
    PlusLife,
    RechargeRockets,
    UpgradeRocketAmmo,
    UpgradeShieldRechargeRate,
    UpgradeShieldCapacity
}