﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ShopItemView : BaseDataContextView<ShopItem>
{
    [SerializeField] private float itemImageScaleDuration = 0.5f;
    [SerializeField] private Image itemImage;
    [SerializeField] private TMP_Text displayNameText;
    [SerializeField] private TMP_Text buyButtonText;
    [SerializeField] private Button buyButton;

    private void Awake()
    {
        Assert.IsNotNull(itemImage);
        Assert.IsNotNull(displayNameText);
        Assert.IsNotNull(buyButtonText);
        Assert.IsNotNull(buyButton);
    }

    protected override void OnInitialize()
    {
        base.OnInitialize();

        buyButton.onClick.AddListener(OnBuyButtonClicked);
        displayNameText.text = ContextData.DisplayName;
        buyButtonText.text = string.Format(Constants.PRICE_STRING_TEMPLATE, ContextData.Price);

        itemImage.transform.localScale = Vector3.zero;
        AddressableHelper.LoadSprite(ContextData.ShopItemType.ToString(), itemImage, OnItemImageLoaded);
    }

    private void OnItemImageLoaded()
    {
        itemImage.transform.DOScale(Vector3.one, itemImageScaleDuration).SetUpdate(true);
    }

    private void OnDestroy()
    {
        buyButton.onClick.RemoveListener(OnBuyButtonClicked);
    }

    private void OnBuyButtonClicked()
    {
        ContextData.Buy();
    }
}
