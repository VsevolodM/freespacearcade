﻿using System;
using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class ProjectileModel : ICloneable
{
    [JsonIgnore] public int OwnerId { get; private set; }
    [JsonProperty] public int Damage { get; private set; }
    [JsonProperty] public float MoveSpeed { get; private set; }
    [JsonProperty] public float ExplodeRadius { get; private set; }
    [JsonProperty] public int ExplodeDamage { get; private set; }
    [JsonProperty] public float SelfDestructionDelay { get; private set; }

    [JsonIgnore] public Pool<ProjectileView> Pool { get; private set; }

    public void Initialize(int ownerId, Pool<ProjectileView> pool)
    {
        OwnerId = ownerId;
        Pool = pool;
    }

    public object Clone()
    {
        return this.DeepClone();
    }

    public override string ToString()
    {
        return
            $"OwnerId: {OwnerId}, Damage: {Damage}, MoveSpeed: {MoveSpeed}, ExplodeRadius: {ExplodeRadius}, ExplodeDamage: {ExplodeDamage}, SelfDestructionDelay: {SelfDestructionDelay}";
    }
}