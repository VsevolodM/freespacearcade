﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class ProjectileView : BaseDataContextView<ProjectileModel>, IPoolable
{
    private Coroutine moveCoroutine;
    private Coroutine dissapearCoroutine;

    protected override void OnInitialize()
    {
        base.OnInitialize();

        this.SafeStopCoroutine(moveCoroutine);

        moveCoroutine = StartCoroutine(Move());

        dissapearCoroutine = CoroutinesHelper.CoroutinesHolder.ExecuteWithDelay(ContextData.SelfDestructionDelay, ReturnToPool);
    }

    private void OnTriggerEnter2D(Collider2D collider2D)
    {
        var ship = collider2D.GetComponent<BaseShipView>();

        if (ship != null && ship.ContextData.Id != ContextData.OwnerId)
        {
            ship.ContextData.ApplyDamage(ContextData.Damage, ContextData.OwnerId);
        }

        ApplyExplodeDamage();
        ReturnToPool();
    }

    private void OnDisable()
    {
        this.SafeStopCoroutine(moveCoroutine);
        CoroutinesHelper.CoroutinesHolder.SafeStopCoroutine(dissapearCoroutine);
    }

    private IEnumerator Move()
    {
        while (true)
        {
            transform.position += transform.up * ContextData.MoveSpeed;
            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
    }

    public void ReturnToPool()
    {
        ContextData.Pool.ReturnItem(this);
    }

    private void ApplyExplodeDamage()
    {
        if (ContextData.ExplodeRadius > 0)
        {
            Collider2D[] overlapResults = new Collider2D[App.AppModel.RoundsManager.Enemies.Count];
            int collidersAmount = Physics2D.OverlapCircleNonAlloc(transform.position, ContextData.ExplodeRadius, overlapResults,
                Constants.SHIPS_LAYER_MASK);

            if (collidersAmount > 0)
            {
                var ships = overlapResults.Where(col => col != null).Select(col => col.GetComponent<BaseShipView>()).Where(sh => sh != null);

                foreach (var ship in ships)
                {
                    ship.ContextData.ApplyDamage(ContextData.ExplodeDamage, ContextData.OwnerId);
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (ContextData.ExplodeRadius > 0f)
        {
            Gizmos.color = Constants.GIZMOS_PROJECTILE_EXPLOSION_COLOR;
            Gizmos.DrawSphere(transform.position, ContextData.ExplodeRadius);
        }
    }
}