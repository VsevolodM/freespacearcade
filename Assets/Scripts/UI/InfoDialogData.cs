﻿using System;

public struct InfoDialogData
{
    public string Title { get; }
    public string Message { get; }
    public Action OkAction { get; }

    public InfoDialogData(string title, string message, Action okAction) : this()
    {
        Title = title;
        Message = message;
        OkAction = okAction;
    }

    public InfoDialogData(string title, string message) : this()
    {
        Title = title;
        Message = message;
    }
}