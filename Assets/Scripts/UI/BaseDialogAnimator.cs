﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

public abstract class BaseDialogAnimator : MonoBehaviour
{
    [SerializeField] protected RectTransform body;

    protected virtual void Awake()
    {
        Assert.IsNotNull(body);
    }

    protected virtual void Reset()
    {
        body = transform.GetChild(0).GetComponent<RectTransform>();
    }

    public abstract void PlayShowAnimation(Action callback = null);
    public abstract void PlayHideAnimation(Action callback = null);
    public abstract void PlayShowAnimationInstant(Action callback = null);
    public abstract void PlayHideAnimationInstant(Action callback = null);
}
