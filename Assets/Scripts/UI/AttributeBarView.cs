﻿using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class AttributeBarView : BaseDataContextView<LimitedAttributeModel>
{
    [SerializeField] protected Slider slider;
    [SerializeField] protected TMP_Text valuesText;

    protected virtual void Awake()
    {
        Assert.IsNotNull(slider);
        Assert.IsNotNull(valuesText);
        slider.interactable = false;
    }

    protected override void OnInitialize()
    {
        base.OnInitialize();
        ContextData.OnChangedEvent += Refresh;
        Refresh();
    }

    protected virtual void OnDestroy()
    {
        ContextData.OnChangedEvent -= Refresh;
    }

    public void Refresh()
    {
        slider.value = ContextData.Percentage;
        valuesText.text = string.Format("{0}/{1}", ContextData.CurrentValue, ContextData.MaxValue);
    }
}
