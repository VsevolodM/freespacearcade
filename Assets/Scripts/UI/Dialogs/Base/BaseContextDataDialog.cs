﻿using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class BaseContextDataDialog<T> : BaseDialog
{
    public T ContextData { get; private set; }

    public void Initialize(T data)
    {
        ContextData = data;
        OnInitialize();
    }

    protected virtual void OnInitialize(){}
}
