﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas), typeof(CanvasGroup))]
public class BaseDialog : MonoBehaviour
{
    public event Action OnClosedEvent;

    [SerializeField] private bool canBeClosedByOverlay = true;
    [SerializeField] private Button overlayButton;
    [SerializeField] private bool pause;

    private float originalTimeScale;

    public CanvasGroup CanvasGroup { get; private set; }

    private Canvas canvas;

    public Canvas Canvas
    {
        get
        {
            if (canvas == null)
            {
                canvas = GetComponent<Canvas>();
            }

            return canvas;
        }
    }

    protected virtual void Awake()
    {
        Canvas.worldCamera = Camera.main;

        CanvasGroup = GetComponent<CanvasGroup>();

        if (canBeClosedByOverlay)
        {
            overlayButton.onClick.AddListener(OnOverlayButtonClicked);
        }

        if (pause)
        {
            originalTimeScale = Time.timeScale;
            Time.timeScale = 0f;
        }
    }

    protected virtual void OnDestroy()
    {
        if (pause)
        {
            Time.timeScale = originalTimeScale;
        }

        OnClosedEvent?.Invoke();
    }

    private void Reset()
    {
        gameObject.name = GetType().Name;
    }

    private void OnOverlayButtonClicked()
    {
        if (canBeClosedByOverlay)
        {
            Close();
        }
    }

    public void Close()
    {
        UIManager.CloseDialog(gameObject);

        if (overlayButton != null)
        {
            overlayButton.onClick.RemoveListener(OnOverlayButtonClicked);
        }
    }
}
