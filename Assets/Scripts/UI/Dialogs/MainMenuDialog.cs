﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class MainMenuDialog : BaseDialog
{
    [SerializeField] private Button playButton;
    [SerializeField] private Button highscoresButton;
    [SerializeField] private Button quitButton;

    protected override void Awake()
    {
        base.Awake();

        Assert.IsNotNull(playButton);
        Assert.IsNotNull(highscoresButton);
        Assert.IsNotNull(quitButton);

        playButton.onClick.AddListener(OnPlayButtonClicked);
        highscoresButton.onClick.AddListener(OnHighscoresButtonClicked);
        quitButton.onClick.AddListener(OnQuitButtonClicked);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        playButton.onClick.RemoveListener(OnPlayButtonClicked);
        highscoresButton.onClick.RemoveListener(OnHighscoresButtonClicked);
        quitButton.onClick.RemoveListener(OnQuitButtonClicked);
    }

    private void OnHighscoresButtonClicked()
    {
        App.AppService.NavigationService.OpenHighScoresDialog(App.AppModel.HighscoreManager);
    }

    private void OnPlayButtonClicked()
    {
        OnClosedEvent += OnClosed;

        void OnClosed()
        {
            App.AppModel.StartGame();
            OnClosedEvent -= OnClosed;
        }

        Close();
    }

    private void OnQuitButtonClicked()
    {
        Application.Quit();
    }
}
