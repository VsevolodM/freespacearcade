﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class PauseDialog : BaseDialog
{
    [SerializeField] private Button closeButton;
    [SerializeField] private Button continueButton;
    [SerializeField] private Button mainMenuButton;
    [SerializeField] private Button highscoresButton;
    [SerializeField] private Button quitButton;

    protected override void Awake()
    {
        base.Awake();

        Assert.IsNotNull(closeButton);
        Assert.IsNotNull(continueButton);
        Assert.IsNotNull(mainMenuButton);
        Assert.IsNotNull(highscoresButton);
        Assert.IsNotNull(quitButton);

        closeButton.onClick.AddListener(OnCloseButtonClicked);
        continueButton.onClick.AddListener(OnContinueButtonClicked);
        mainMenuButton.onClick.AddListener(OnMainMenuButtonClicked);
        highscoresButton.onClick.AddListener(OnHighscoresButtonClicked);
        quitButton.onClick.AddListener(OnQuitButtonClicked);
    }

    private void OnCloseButtonClicked()
    {
        Close();
    }

    private void OnContinueButtonClicked()
    {
        Close();
    }

    private void OnMainMenuButtonClicked()
    {
        Close();
        App.AppModel.EndGame();
        App.AppService.NavigationService.OpenMainMenuDialog();
    }

    private void OnHighscoresButtonClicked()
    {
        App.AppService.NavigationService.OpenHighScoresDialog(App.AppModel.HighscoreManager);
    }

    private void OnQuitButtonClicked()
    {
        Application.Quit();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        closeButton.onClick.RemoveListener(OnCloseButtonClicked);
        continueButton.onClick.RemoveListener(OnContinueButtonClicked);
        mainMenuButton.onClick.RemoveListener(OnMainMenuButtonClicked);
        highscoresButton.onClick.RemoveListener(OnHighscoresButtonClicked);
        quitButton.onClick.RemoveListener(OnQuitButtonClicked);
    }
}
