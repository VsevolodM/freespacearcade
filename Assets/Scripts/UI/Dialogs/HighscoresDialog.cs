﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class HighscoresDialog : BaseContextDataDialog<HighscoreManager>
{
    [SerializeField] private Button closeButton;
    [SerializeField] private ViewSpawner highscoreViewSpawner;
    [SerializeField] private GameObject hasEntriesGameObject;
    [SerializeField] private GameObject hasNoEntriesGameObject;

    protected override void Awake()
    {
        base.Awake();
        Assert.IsNotNull(closeButton);
        Assert.IsNotNull(highscoreViewSpawner);
        Assert.IsNotNull(hasEntriesGameObject);
        Assert.IsNotNull(hasNoEntriesGameObject);
    }

    protected override void OnInitialize()
    {
        base.OnInitialize();
        RefreshedEntries();
        closeButton.onClick.AddListener(OnCloseButtonClicked);
        ContextData.OnEntriesChangedEvent += RefreshedEntries;
    }

    private void RefreshedEntries()
    {
        bool hasHighscoreEntries = ContextData.Entries.Count > 0;

        if (hasHighscoreEntries)
        {
            highscoreViewSpawner.Spawn<HighscoreEntryModel, HighscoreEntryView>(ContextData.Entries);
        }

        hasEntriesGameObject.SetActive(hasHighscoreEntries);
        hasNoEntriesGameObject.SetActive(!hasHighscoreEntries);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        closeButton.onClick.RemoveListener(OnCloseButtonClicked);
        ContextData.OnEntriesChangedEvent -= RefreshedEntries;
    }

    private void OnCloseButtonClicked()
    {
        Close();
    }
}
