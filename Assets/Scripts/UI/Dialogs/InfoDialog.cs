using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoDialog : BaseContextDataDialog<InfoDialogData>
{
    [SerializeField] private TMP_Text titleText;
    [SerializeField] private TMP_Text messageText;
    [SerializeField] private TMP_Text okButtonText;
    [SerializeField] private Button okButton;

    protected override void OnInitialize()
    {
        base.OnInitialize();
		titleText.text = ContextData.Title;
        messageText.text = ContextData.Message;

        okButton.onClick.AddListener(OnOkButtonClick);
    }

    private void OnOkButtonClick()
    {
        ContextData.OkAction?.Invoke();
        Close();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        okButton.onClick.RemoveListener(OnOkButtonClick);
    }
}
