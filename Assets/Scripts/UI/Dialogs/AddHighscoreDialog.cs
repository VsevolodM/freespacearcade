﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class AddHighscoreDialog : BaseContextDataDialog<int>
{
    public event Action<HighscoreEntryModel> OnAcceptedEvent;

    [SerializeField] private TMP_InputField inputField;
    [SerializeField] private Button acceptButton;
    [SerializeField] private Button closeButton;

    private DateTime dateTime;

    protected override void Awake()
    {
        base.Awake();
        Assert.IsNotNull(inputField);
    }

    protected override void OnInitialize()
    {
        base.OnInitialize();

        dateTime = DateTime.Now;

        inputField.characterLimit = Constants.HIGHSCORE_NAME_MAX_LENGTH;

        acceptButton.onClick.AddListener(OnAcceptButton);
        closeButton.onClick.AddListener(OnCloseButton);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        acceptButton.onClick.RemoveListener(OnAcceptButton);
        closeButton.onClick.RemoveListener(OnCloseButton);
    }

    private void OnAcceptButton()
    {
        var highScoreEntryModel = new HighscoreEntryModel(-1, inputField.text, ContextData, dateTime);
        OnAcceptedEvent?.Invoke(highScoreEntryModel);
        Close();
    }

    private void OnCloseButton()
    {
        Close();
    }
}
