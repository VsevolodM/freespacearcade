﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ShopDialog : BaseDialog
{
    [SerializeField] private Button closeButton;
    [SerializeField] private ViewSpawner viewSpawner;

    protected override void Awake()
    {
        base.Awake();
        Assert.IsNotNull(closeButton);
        Assert.IsNotNull(viewSpawner);

        var shopItems = App.AppModel.ShopModel.ShopSettings.Items;

        for (int i = 0; i < shopItems.Length; i++)
        {
            shopItems[i].SetBoughtItemsAmountToZero();
        }

        viewSpawner.Spawn<ShopItem, ShopItemView>(shopItems);
        closeButton.onClick.AddListener(OnCloseButtonClicked);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        closeButton.onClick.RemoveListener(OnCloseButtonClicked);
    }

    private void OnCloseButtonClicked()
    {
        Close();
    }
}
