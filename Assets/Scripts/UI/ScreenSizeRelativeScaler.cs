﻿using UnityEngine;

[RequireComponent(typeof(RectTransform))]
[ExecuteInEditMode]
public class ScreenSizeRelativeScaler : MonoBehaviour
{
	[SerializeField] private ScaleType scaleType;
	[SerializeField] private float minScale = 0.1f;
	[SerializeField] private float maxScale = 1f;
	[SerializeField] private Vector2 targetResolution = new Vector2(2048, 1024);

	private void Awake()
	{
		Refresh();
	}

#if UNITY_EDITOR
	private void Update()
	{
		Refresh();
	}
#endif

	private void Refresh()
	{
		Canvas[] canvas = transform.GetComponentsInParent<Canvas>();
		Canvas topmost = canvas[canvas.Length - 1];

		var topMostRectTransform = topmost.GetComponent<RectTransform>();

		float xAspectRatio = topMostRectTransform.GetWidth() / targetResolution.x;
		float yAspectRatio = topMostRectTransform.GetHeight() / targetResolution.y;

		float scale = 1;

		if (scaleType == ScaleType.Min)
		{
			scale = Mathf.Min(xAspectRatio, yAspectRatio);
		}
		else if (scaleType == ScaleType.Max)
		{
			scale = Mathf.Max(xAspectRatio, yAspectRatio);
		}

		scale = Mathf.Clamp(scale, minScale, maxScale);
		
		transform.localScale = Vector3.one * scale;
	}
}

public enum ScaleType
{
	Min,
	Max
}