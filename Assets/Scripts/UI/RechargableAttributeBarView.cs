﻿using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

public class RechargableAttributeBarView : BaseDataContextView<RechargableAttributeModel>
{
    [SerializeField] private TMP_Text rechargeDelayText;
    [SerializeField] private TMP_Text unitRechargeTimeText;
    [SerializeField] private AttributeBarView attributeBarView;

    private void Awake()
    {
        Assert.IsNotNull(rechargeDelayText);
        Assert.IsNotNull(unitRechargeTimeText);   
        Assert.IsNotNull(attributeBarView);   
    }

    protected override void OnInitialize()
    {
        base.OnInitialize();

        attributeBarView.Initialize(ContextData.CapacityAttributeModel);

        ContextData.CapacityAttributeModel.Upgradable.OnLevelChangedEvent += Refresh;
        ContextData.RechargeAttributeModel.Upgradable.OnLevelChangedEvent += Refresh;

        Refresh();
    }

    private void Refresh()
    {
        attributeBarView.Refresh();
        rechargeDelayText.text = string.Format("Delay: {0}s", ContextData.RechargeDelay);
        unitRechargeTimeText.text = string.Format("1/{0}s", ContextData.RechargeAttributeModel.CurrentValue);
    }

    private void OnDestroy()
    {
        ContextData.CapacityAttributeModel.Upgradable.OnLevelChangedEvent -= Refresh;
        ContextData.RechargeAttributeModel.Upgradable.OnLevelChangedEvent -= Refresh;
    }
}
