﻿using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class InGameUIManager : BaseDataContextView<AppModel>
{
    [SerializeField] private TMP_Text livesText;
    [SerializeField] private TMP_Text rocketsAmountText;
    [SerializeField] private TMP_Text enemiesKilledText;
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private TMP_Text coinsAmountText;
    [SerializeField] private TMP_Text timeLeftText;
    [SerializeField] private AttributeBarView healthBarView;
    [SerializeField] private RechargableAttributeBarView shieldsBarView;
    [SerializeField] private Button pauseButton;

    private Canvas canvas;

    private void Awake()
    {
        Assert.IsNotNull(livesText);
        Assert.IsNotNull(rocketsAmountText);
        Assert.IsNotNull(enemiesKilledText);
        Assert.IsNotNull(scoreText);
        Assert.IsNotNull(coinsAmountText);
        Assert.IsNotNull(timeLeftText);
        Assert.IsNotNull(healthBarView);
        Assert.IsNotNull(shieldsBarView);
        Assert.IsNotNull(pauseButton);

        canvas = GetComponent<Canvas>();
    }

    protected override void OnInitialize()
    {
        base.OnInitialize();

        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = Camera.main;

        healthBarView.Initialize(ContextData.AppSettings.PlayerModel.HealthModel);
        shieldsBarView.Initialize(ContextData.AppSettings.PlayerModel.ShieldsModel);

        pauseButton.onClick.AddListener(OnPauseButtonClicked);

        OnRocketsGunModelChanged();
        OnScoreChanged();
        OnTimerChanged();
        OnCoinsAmountChanged();
        OnLivesAmountChanged();
        OnEnemiesKilledChanged();

        ContextData.AppSettings.PlayerModel.RocketGun.UpgradableAmmoModel.OnChangedEvent += OnRocketsGunModelChanged;
        ContextData.AppSettings.PlayerModel.RocketGun.UpgradableAmmoModel.Upgradable.OnLevelChangedEvent += OnRocketsGunModelChanged;
        ContextData.OnScoreChangedEvent += OnScoreChanged;
        ContextData.OnEnemiesKilledChangedEvent += OnEnemiesKilledChanged;
        ContextData.AppSettings.PlayerModel.LivesModel.OnChangedEvent += OnLivesAmountChanged;
        ContextData.OnCoinsAmountChangedEvent += OnCoinsAmountChanged;
        ContextData.RoundsManager.OnRoundEndTimeChangedEvent += OnTimerChanged;
    }

    private void OnRocketsGunModelChanged()
    {
        var maxAmmoAmount = ContextData.AppSettings.PlayerModel.RocketGun.UpgradableAmmoModel.MaxValue;
        var currentAmmoAmount = ContextData.AppSettings.PlayerModel.RocketGun.UpgradableAmmoModel.CurrentValue;
        rocketsAmountText.text = string.Format("Rockets: {0}/{1}", currentAmmoAmount, maxAmmoAmount);
    }

    private void OnPauseButtonClicked()
    {
        App.AppService.NavigationService.OpenPauseDialog();
    }

    private void OnScoreChanged()
    {
        scoreText.text = string.Format("Score: {0}", ContextData.Score);
    }

    private void OnTimerChanged()
    {
        timeLeftText.text = string.Format("Time left: {0}", ContextData.RoundsManager.TimeToEndOfRoundSec);
    }

    private void OnCoinsAmountChanged()
    {
        coinsAmountText.text = string.Format("{0} <sprite=0>", ContextData.CoinsAmount);
    }

    private void OnLivesAmountChanged()
    {
        livesText.text = string.Format("Lives {0}", ContextData.AppSettings.PlayerModel.LivesModel.CurrentValue);
    }

    private void OnEnemiesKilledChanged()
    {
        enemiesKilledText.text = string.Format("Kills {0}", ContextData.EnemiesKilled);
    }

    private void OnDestroy()
    {
        pauseButton.onClick.RemoveListener(OnPauseButtonClicked);

        ContextData.AppSettings.PlayerModel.RocketGun.UpgradableAmmoModel.OnChangedEvent -= OnRocketsGunModelChanged;
        ContextData.AppSettings.PlayerModel.RocketGun.UpgradableAmmoModel.Upgradable.OnLevelChangedEvent -= OnRocketsGunModelChanged;
        ContextData.OnEnemiesKilledChangedEvent -= OnEnemiesKilledChanged;
        ContextData.AppSettings.PlayerModel.LivesModel.OnChangedEvent -= OnLivesAmountChanged;
        ContextData.OnCoinsAmountChangedEvent -= OnCoinsAmountChanged;
        ContextData.RoundsManager.OnRoundEndTimeChangedEvent -= OnTimerChanged;
    }
}
