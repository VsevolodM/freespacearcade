﻿using UnityEngine;

public abstract class BaseDataContextView<T> : MonoBehaviour
{
    public T ContextData { get; private set; }

    public void Initialize(T contextData)
    {
        ContextData = contextData;
        OnInitialize();
    }

    protected virtual void OnInitialize(){}
}
