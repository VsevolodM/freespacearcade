﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class UIManager
{
    private static List<GameObject> dialogs = new List<GameObject>();

    private static readonly Transform dialogsParent;

    private static int currentDialogCanvasOrder = 500;

    static UIManager()
    {
        dialogsParent = new GameObject(Constants.DIALOGS_PARENT_NAME).transform;
        Object.DontDestroyOnLoad(dialogsParent.gameObject);
    }

    public static V OpenView<V, M>(M model) where V: BaseDataContextView<M>
    {
        string path = Path.Combine(Constants.UI_ROOT_PATH, typeof(V).Name);
        var viewGameObject = SpawnView(path, null);
        var view = viewGameObject.GetComponent<V>();
        view.Initialize(model);
        return view;
    }

    public static T OpenDialog<T>() where T: BaseDialog
    {
        string path = Path.Combine(Constants.DIALOGS_ROOT_PATH, typeof(T).Name);

        var dialog = SpawnView(path, dialogsParent);
        dialogs.Add(dialog);

        var dialogComponent = dialog.GetComponent<BaseDialog>();

        var dialogAnimator = dialog.GetComponent<BaseDialogAnimator>();

        void OnPlayerAnimationCompleted()
        {
            dialogComponent.CanvasGroup.interactable = true;
        }

        if (dialogAnimator != null)
        {
            dialogComponent.CanvasGroup.interactable = false;
            dialogAnimator.PlayShowAnimation(OnPlayerAnimationCompleted);
        }
        else
        {
            OnPlayerAnimationCompleted();
        }


        dialogComponent.Canvas.sortingOrder = currentDialogCanvasOrder;
        currentDialogCanvasOrder++;
        return dialog.GetComponent<T>();
    }

    private static GameObject SpawnView(string path, Transform parent)
    {
        var dialogPrefab = Resources.Load<GameObject>(path);
        var dialog = Object.Instantiate(dialogPrefab, parent);
        dialog.transform.localScale = Vector3.one;
        dialog.transform.position = Vector3.zero;
        dialog.gameObject.RemoveCloneFromName();
        return dialog.gameObject;
    }

    public static void CloseDialog(GameObject dialog)
    {
        int dialogIndex = dialogs.IndexOf(dialog);

        if (dialogIndex > -1)
        {
            var dialogGameObject = dialogs[dialogIndex];
            var dialogAnimator = dialogGameObject.GetComponent<BaseDialogAnimator>();
            dialogs.RemoveAt(dialogIndex);

            var dialogComponent = dialogGameObject.GetComponent<BaseDialog>();

            void DestroyDialog()
            {
                Object.Destroy(dialogGameObject);
            }

            if (dialogAnimator == null)
            {
                DestroyDialog();
            }
            else
            {
                dialogComponent.CanvasGroup.interactable = false;
                dialogAnimator.PlayHideAnimation(DestroyDialog);
            }
        }

        currentDialogCanvasOrder--;
    }
}
