﻿using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class RoundsSettings
{
    [JsonProperty] public float RoundDurationSec { get; private set; }
    [JsonProperty] public int BaseEnemiesAmount { get; private set; }
    [JsonProperty] public int EnemiesPerRound { get; private set; }
    [JsonProperty] public int MinScorePerEnemy { get; private set; }
    [JsonProperty] public int MaxScorePerEnemy { get; private set; }

    public override string ToString()
    {
        return $"RoundDurationSec: {RoundDurationSec}, BaseEnemiesAmount: {BaseEnemiesAmount}, EnemiesPerRound: {EnemiesPerRound}, MinScorePerEnemy: {MinScorePerEnemy}, MaxScorePerEnemy: {MaxScorePerEnemy}";
    }
}
