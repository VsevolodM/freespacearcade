﻿public static class App
{
    public static AppModel AppModel { get; private set; }
    public static AppService AppService { get; private set; }

    static App()
    {
        AppModel = new AppModel();
        AppService = new AppService();
    }
}
