﻿using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class GeneralGameSettings
{
    [JsonProperty] public float CoinLifespanSec { get; private set; }
    [JsonProperty] public ProjectileModel ProjectileModel { get; private set; }
    [JsonProperty] public ProjectileModel RocketModel { get; private set; }
    [JsonProperty] public LevelingDataManager RocketLevelingDataManager { get; private set; }
    [JsonProperty] public LevelingDataManager ShieldCapacityLevelingManager { get; private set; }
    [JsonProperty] public LevelingDataManager ShieldRechargeRateLevelingManager { get; private set; }

    public override string ToString()
    {
        return $"CoinLifespanSec: {CoinLifespanSec}, ProjectileModel: {ProjectileModel}, RocketModel: {RocketModel}, RocketLevelingDataManager: {RocketLevelingDataManager}, ShieldCapacityLevelingManager: {ShieldCapacityLevelingManager}, ShieldRechargeRateLevelingManager: {ShieldRechargeRateLevelingManager}";
    }
}