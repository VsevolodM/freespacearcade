﻿using Newtonsoft.Json;
using System;
using UnityEngine;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class AttributeModel
{
    public event Action OnChangedEvent;
 
    [JsonProperty] public virtual float CurrentValue { get; private set; }

    public float LastDelta { get; private set; }

    public void Add(float value)
    {
        SetCurrentValue(CurrentValue + value);
    }

    public virtual void Subtract(float value)
    {
        SetCurrentValue(CurrentValue - value);
    }

    public virtual void SetCurrentValue(float value)
    {
        if (!Mathf.Approximately(value, CurrentValue))
        {
            LastDelta = value - CurrentValue;
            CurrentValue = value;
            OnChangedEvent?.Invoke();
        }
    }

    public override string ToString()
    {
        return $"CurrentValue: {CurrentValue}";
    }
}
