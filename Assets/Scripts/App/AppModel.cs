﻿using Newtonsoft.Json;
using System;
using System.IO;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class AppModel
{
    public event Action OnEnemiesKilledChangedEvent;
    public event Action OnScoreChangedEvent;
    public event Action OnCoinsAmountChangedEvent;

    public AppSettings AppSettings { get; private set; }
    public RoundsManager RoundsManager { get; private set; }
    public ShopModel ShopModel { get; private set; }
    public HighscoreManager HighscoreManager { get; }

    public int Score { get; private set; }
    public int EnemiesKilled { get; private set; }
    public uint CoinsAmount { get; private set; }

    public Pool<ProjectileView> ProjectilePool { get; }
    public Pool<ProjectileView> RocketsPool { get; }
    public Pool<CoinView> CoinsPool { get; }
    public Pool<EnemyShipView> EnemyShipsPool { get; }

    private int onCoinPickedEventId;
    private PlayerShipView spawnedPlayerShipView;
    private readonly BaseShipView playerShipPrefab = Resources.Load<BaseShipView>(Constants.PLAYERSHIP_PREFAB_PATH).GetComponent<BaseShipView>();
    private readonly TextAsset settingsFile = Resources.Load<TextAsset>(Constants.APP_SETTINGS_PATH);
    private readonly GameObject radarCameraPrefab = Resources.Load<GameObject>(Constants.RADAR_CAMERA_PATH);

    private readonly Camera mainCamera;
    private readonly GameObject spawnedBackground;
    private InGameUIManager inGameUIManager;

    public AppModel()
    {
        AudioManager.PlayMusic(Constants.MUSIC_ADDRESSABLE, Constants.DEFAULT_MUSIC_FADE_TIME);

        if (File.Exists(Constants.HIGHSCORE_FILE_PATH))
        {
            string highScoresJson = File.ReadAllText(Constants.HIGHSCORE_FILE_PATH);
            HighscoreManager = JsonConvert.DeserializeObject<HighscoreManager>(highScoresJson);
        }
        else
        {
            HighscoreManager = new HighscoreManager();
        }

        ProjectilePool = new Pool<ProjectileView>(Constants.PROJECTILE_POOL_PARENT_NAME, Constants.PROJECTILE_PREFAB_PATH, 20);
        RocketsPool = new Pool<ProjectileView>(Constants.ROCKETS_POOL_PARENT_NAME, Constants.ROCKETS_PREFAB_PATH, 20);
        CoinsPool = new Pool<CoinView>(Constants.COIN_POOL_PARENT_NAME, Constants.COIN_PREFAB_PATH, 20);
        EnemyShipsPool = new Pool<EnemyShipView>(Constants.SHIP_POOL_PARENT_NAME, Constants.ENEMYSHIP_PREFAB_PATH, 20);

        onCoinPickedEventId = GlobalEventsListener.Register(GlobalEvent.CoinPicked, OnCoinPicked);

        mainCamera = Camera.main;

        var backgroundPrefab = Resources.Load<GameObject>(Constants.BACKGROUND_PREFAB_PATH);
        spawnedBackground = Object.Instantiate(backgroundPrefab, mainCamera.transform);
        spawnedBackground.gameObject.RemoveCloneFromName();
    }

    private void SetManagerForShip(BaseShipModel ship)
    {
        ship.HealthModel.Restore();
        ship.ShieldsModel.CapacityAttributeModel.Upgradable.SetManager(AppSettings.GeneralGameSettings.ShieldCapacityLevelingManager);
        ship.ShieldsModel.RechargeAttributeModel.Upgradable.SetManager(AppSettings.GeneralGameSettings.ShieldRechargeRateLevelingManager);
        ship.RocketGun.UpgradableAmmoModel.Upgradable.SetManager(AppSettings.GeneralGameSettings.RocketLevelingDataManager);

        ship.LivesModel.Restore();
        ship.HealthModel.Restore();
        ship.ShieldsModel.CapacityAttributeModel.Restore();
        ship.RocketGun.UpgradableAmmoModel.Restore();
    }

    public void StartGame()
    {
        AppSettings = JsonConvert.DeserializeObject<AppSettings>(settingsFile.text);
        RoundsManager = new RoundsManager(AppSettings.RoundsSettings, AppSettings.EnemyModel);
        ShopModel = new ShopModel(AppSettings.ShopSettings);
        RoundsManager.OnPlayerDestroyedEnemyEvent += OnPlayerDestroyedEnemy;

        SetManagerForShip(AppSettings.PlayerModel);
        SetManagerForShip(AppSettings.EnemyModel);

        AppSettings.PlayerModel.ProjectileGun.Initialize(Constants.PLAYER_SHIP_ID, ProjectilePool);
        AppSettings.PlayerModel.RocketGun.Initialize(Constants.PLAYER_SHIP_ID, RocketsPool);

        spawnedPlayerShipView = Object.Instantiate(playerShipPrefab, null).GetComponent<PlayerShipView>();
        spawnedPlayerShipView.transform.position = Vector3.zero;
        spawnedPlayerShipView.Initialize(AppSettings.PlayerModel);

        spawnedPlayerShipView.gameObject.RemoveCloneFromName();

        var textureScroller = spawnedBackground.GetComponent<TextureScroller>();
        textureScroller.Initialize(spawnedPlayerShipView.transform);

        mainCamera.GetComponent<CameraFollow>().Initialize(spawnedPlayerShipView.transform);

        var radarCamera = Object.Instantiate(radarCameraPrefab, null);
        radarCamera.gameObject.RemoveCloneFromName();
        var cameraFollow = radarCamera.gameObject.GetComponent<CameraFollow>();
        cameraFollow.Initialize(spawnedPlayerShipView.transform);

        RoundsManager.StartRound();

        inGameUIManager = UIManager.OpenView<InGameUIManager, AppModel>(this);

        AppSettings.PlayerModel.OnDestroyedEvent += OnPlayerShipDestroyed;
    }

    private void OnPlayerShipDestroyed()
    {
        EndGame();
        var highScoresDialog = App.AppService.NavigationService.OpenHighScoresDialog(HighscoreManager);
        highScoresDialog.OnClosedEvent += OnHighScoresDialogClosed;

        var addHighScoreDialog = App.AppService.NavigationService.OpenAddHighScoreDialog(Score);
        addHighScoreDialog.OnAcceptedEvent += OnHighScoreAccepted;

        void OnHighScoresDialogClosed()
        {
            highScoresDialog.OnClosedEvent -= OnHighScoresDialogClosed;
            App.AppService.NavigationService.OpenMainMenuDialog();
        }

        void OnHighScoreAccepted(HighscoreEntryModel entry)
        {
            HighscoreManager.AddNewEntry(entry);
            addHighScoreDialog.OnAcceptedEvent -= OnHighScoreAccepted;
        }
    }

    public void EndGame()
    {
        RoundsManager.Dispose();

        RoundsManager.ResetRoundIndex();
        Object.Destroy(spawnedPlayerShipView.gameObject);

        CoinsPool.GetAllItemsBack();
        EnemyShipsPool.GetAllItemsBack();
        ProjectilePool.GetAllItemsBack();
        RocketsPool.GetAllItemsBack();

        CoinsAmount = 0;
        Score = 0;
        EnemiesKilled = 0;

        RoundsManager.OnPlayerDestroyedEnemyEvent -= OnPlayerDestroyedEnemy;
        Object.Destroy(inGameUIManager.gameObject);
    }

    private void OnCoinPicked()
    {
        CoinsAmount++;
        OnCoinsAmountChangedEvent?.Invoke();
    }

    private void OnPlayerDestroyedEnemy()
    {
        EnemiesKilled++;
        OnEnemiesKilledChangedEvent?.Invoke();

        Score += GetRandomScoreForEnemy();
        OnScoreChangedEvent?.Invoke();
    }

    private int GetRandomScoreForEnemy()
    {
        return Random.Range(AppSettings.RoundsSettings.MinScorePerEnemy, AppSettings.RoundsSettings.MaxScorePerEnemy);
    }

    public void SubtractCoinsAmount(uint amount)
    {
        CoinsAmount -= amount;
        OnCoinsAmountChangedEvent?.Invoke();
    }

    ~AppModel()
    {
        RoundsManager.OnPlayerDestroyedEnemyEvent -= OnPlayerDestroyedEnemy;
        GlobalEventsListener.Unregister(GlobalEvent.CoinPicked, onCoinPickedEventId);
    }
}
