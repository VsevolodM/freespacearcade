﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RoundsManager : IDisposable
{
    public event Action OnRoundEndTimeChangedEvent;
    public event Action OnPlayerDestroyedEnemyEvent;

    public short RoundIndex { get; private set; }

    private readonly RoundsSettings settings;
    private readonly EnemyShipModel enemyShipModel;

    private int currentEnemyShipId = 1;
    private Coroutine timerCoroutine;
    private int enemyDestroyedEventId;

    public int TimeToEndOfRoundSec { get; private set; }

    public List<EnemyShipModel> Enemies { get; } = new List<EnemyShipModel>();

    public RoundsManager(RoundsSettings settings, EnemyShipModel enemyShipModel)
    {
        this.settings = settings;
        this.enemyShipModel = enemyShipModel;

        enemyDestroyedEventId = GlobalEventsListener.Register<BaseShipModel, int>(GlobalEvent.EnemyDestroyed, OnShipDestroyed);
    }

    private void OnShipDestroyed(BaseShipModel ship, int destroyerId)
    {
        if (ship.Id != Constants.PLAYER_SHIP_ID)
        {
            Enemies.Remove((EnemyShipModel)ship);

            if (destroyerId == Constants.PLAYER_SHIP_ID)
            {
                OnPlayerDestroyedEnemyEvent?.Invoke();
            }

            if (Enemies.Count < 1)
            {
                EndRound();
            }
        }
    }

    public void ResetRoundIndex()
    {
        RoundIndex = 0;
    }

    public void StartRound()
    {
        RoundIndex++;
        SpawnEnemiesForWave(RoundIndex);

        CoroutinesHelper.CoroutinesHolder.SafeStopCoroutine(timerCoroutine);
        timerCoroutine = CoroutinesHelper.CoroutinesHolder.StartCoroutine(StartRoundTimer(settings.RoundDurationSec));
    }

    private IEnumerator StartRoundTimer(float secondsToEnd)
    {
        TimeToEndOfRoundSec = (int)secondsToEnd;
        OnRoundEndTimeChangedEvent?.Invoke();

        while (TimeToEndOfRoundSec > 0)
        {
            yield return Constants.WAIT_FOR_ONE_SECOND_DELAY;
            TimeToEndOfRoundSec--;
            OnRoundEndTimeChangedEvent?.Invoke();
        }

        EndRound();
    }

    private void SpawnEnemiesForWave(short roundIndex)
    {
        int enemiesAmount = settings.BaseEnemiesAmount + (roundIndex - 1) * settings.EnemiesPerRound;

        for (int i = 0; i < enemiesAmount; i++)
        {
            var shipModel = (EnemyShipModel)enemyShipModel.Clone();
            shipModel.SetId(currentEnemyShipId);
            Enemies.Add(shipModel);

            var shipView = App.AppModel.EnemyShipsPool.GetItem(Random.insideUnitCircle * enemyShipModel.ShipScanRadius * 3.5f);
            shipView.Initialize(shipModel);

            currentEnemyShipId++;
        }
    }

    private void EndRound()
    {
        void OnCoinLifespanPassed()
        {
            var shopDialog = App.AppService.NavigationService.OpenShopDialog();
            shopDialog.OnClosedEvent += OnShopDialogClosed;
        }

        CoroutinesHelper.CoroutinesHolder.ExecuteWithDelay(App.AppModel.AppSettings.GeneralGameSettings.CoinLifespanSec,
            OnCoinLifespanPassed);
    }

    private void OnShopDialogClosed()
    {
        StartRound();
    }

    public void Dispose()
    {
        GlobalEventsListener.Unregister(GlobalEvent.EnemyDestroyed, enemyDestroyedEventId);
        GC.SuppressFinalize(this);
    }
}
