﻿public class NavigationService
{
    public MainMenuDialog OpenMainMenuDialog()
    {
        var mainMenuDialog = UIManager.OpenDialog<MainMenuDialog>();
        return mainMenuDialog;
    }

    public ShopDialog OpenShopDialog()
    {
        return UIManager.OpenDialog<ShopDialog>(); ;
    }

    public PauseDialog OpenPauseDialog()
    {
        return UIManager.OpenDialog<PauseDialog>(); ;
    }

    public InfoDialog OpenInfoDialog(InfoDialogData infoDialogData)
    {
        var infoDialog = UIManager.OpenDialog<InfoDialog>();
        infoDialog.Initialize(infoDialogData);
        return infoDialog;
    }

    public AddHighscoreDialog OpenAddHighScoreDialog(int score)
    {
        var addHighScoreDialog = UIManager.OpenDialog<AddHighscoreDialog>();
        addHighScoreDialog.Initialize(score);
        return addHighScoreDialog;
    }

    public HighscoresDialog OpenHighScoresDialog(HighscoreManager highScoreManager)
    {
        var highscoresDialog = UIManager.OpenDialog<HighscoresDialog>();
        highscoresDialog.Initialize(highScoreManager);
        return highscoresDialog;
    }
}
