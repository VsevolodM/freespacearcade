﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;

[JsonObject(MemberSerialization.OptIn)]
public class HighscoreManager
{
    public event Action OnEntriesChangedEvent;

    [JsonProperty] public List<HighscoreEntryModel> Entries { get; private set; }

    public HighscoreManager()
    {
        Entries = new List<HighscoreEntryModel>();
    }

    [OnDeserialized]
    private void OnDeserializedMethod(StreamingContext context)
    {
        if (Entries != null && Entries.Count > 0)
        {
            Entries = Entries.OrderByDescending(entry => entry.Score).ToList();
        }
    }

    public void AddNewEntry(HighscoreEntryModel entry)
    {
        Entries.Add(entry);

        Entries = Entries.OrderByDescending(en => en.Score).ToList();

        for (int i = 0; i < Entries.Count; i++)
        {
            Entries[i].SetPlace(i);
        }

        OnEntriesChangedEvent?.Invoke();

        string jsonString = JsonConvert.SerializeObject(this);
        File.WriteAllText(Constants.HIGHSCORE_FILE_PATH, jsonString);
    }
}
