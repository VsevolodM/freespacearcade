﻿using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

public class HighscoreEntryView : BaseDataContextView<HighscoreEntryModel>
{
    [SerializeField] private TMP_Text placeText;
    [SerializeField] private TMP_Text nameText;
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private TMP_Text dateText;

    private void Awake()
    {
        Assert.IsNotNull(placeText);
        Assert.IsNotNull(nameText);
        Assert.IsNotNull(scoreText);
        Assert.IsNotNull(dateText);
    }

    protected override void OnInitialize()
    {
        base.OnInitialize();
        placeText.text = (ContextData.Place + 1).ToString();
        nameText.text = ContextData.Name;
        scoreText.text = ContextData.Score.ToString();
        dateText.text = ContextData.DateTime.ToString(Constants.HIGHSCORE_DATE_FORMAT);
    }
}
