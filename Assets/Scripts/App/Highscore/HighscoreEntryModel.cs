﻿using System;
using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class HighscoreEntryModel
{
    [JsonProperty] public int Place { get; private set; }
    [JsonProperty] public string Name { get; private set; }
    [JsonProperty] public int Score { get; private set; }
    [JsonProperty] public DateTime DateTime { get; private set; }

    public HighscoreEntryModel(int place, string name, int score, DateTime dateTime)
    {
        Place = place;
        Name = name;
        Score = score;
        DateTime = dateTime;
    }

    public void SetPlace(int place)
    {
        Place = place;
    }

    public void SetName(string name)
    {
        Name = name;
    }

    public override string ToString()
    {
        return $"Place: {Place}, Name: {Name}, Score: {Score}, DateTime: {DateTime}";
    }
}
