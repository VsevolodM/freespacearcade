﻿using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class ShopSettings
{
    [JsonProperty] public ShopItem[] Items { get; private set; }

    public override string ToString()
    {
        return $"Items: {Items.ToStringWithSeparator()}";
    }
}