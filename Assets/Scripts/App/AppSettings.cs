﻿using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class AppSettings
{
    [JsonProperty] public GeneralGameSettings GeneralGameSettings { get; private set; }
    [JsonProperty] public RoundsSettings RoundsSettings { get; private set; }
    [JsonProperty] public BaseShipModel PlayerModel { get; private set; }
    [JsonProperty] public EnemyShipModel EnemyModel { get; private set; }
    [JsonProperty] public ShopSettings ShopSettings { get; private set; }

    public override string ToString()
    {
        return $"GeneralGameSettings: {GeneralGameSettings}, RoundsSettings: {RoundsSettings}, ShopSettings: {ShopSettings}, PlayerModel: {PlayerModel}, EnemyModel: {EnemyModel}";
    }
}