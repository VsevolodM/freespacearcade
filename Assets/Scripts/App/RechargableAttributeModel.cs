﻿using System;
using System.Collections;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using UnityEngine;

[JsonObject(MemberSerialization.OptIn), Serializable]
public class RechargableAttributeModel : AttributeModel
{
    [JsonProperty] public float RechargeDelay { get; private set; }
    [JsonProperty] public UpgradableAttributeModel RechargeAttributeModel { get; private set; }
    [JsonProperty] public LimitedUpgradableAttributeModel CapacityAttributeModel { get; private set; }

    [NonSerialized] private WaitForSeconds rechargeDelay;
    [NonSerialized] private Coroutine recoveringCoroutine;

    private void OnCapacityChanged()
    {
        if (!CapacityAttributeModel.IsFull && CapacityAttributeModel.LastDelta < 0)
        {
            CoroutinesHelper.CoroutinesHolder.SafeStopCoroutine(recoveringCoroutine);
            recoveringCoroutine = CoroutinesHelper.CoroutinesHolder.StartCoroutine(StartRecovering());
        }
    }

    [OnDeserialized]
    private void OnDeserializedMethod(StreamingContext context)
    {
        CapacityAttributeModel.OnChangedEvent += OnCapacityChanged;
        rechargeDelay = new WaitForSeconds(RechargeDelay);
    }

    private IEnumerator StartRecovering()
    {
        var unitRecoverDelay = new WaitForSeconds(RechargeAttributeModel.CurrentValue);

        yield return rechargeDelay;

        while (!CapacityAttributeModel.IsFull)
        {
            CapacityAttributeModel.Add(1);
            yield return unitRecoverDelay;
        }
    }
}
