﻿using System.IO;
using UnityEngine;

public static class Constants
{
    public static string MUSIC_ADDRESSABLE = "Music";
    public static float DEFAULT_MUSIC_FADE_TIME = 0.3f;

    public static string BACKGROUND_PREFAB_PATH = "Prefabs/SpaceBackground";
    public static string RADAR_CAMERA_PATH = "Prefabs/RadarCamera";
    public static string HIGHSCORE_DATE_FORMAT = "HH:mm:ss MM.dd.yyyy";
    public static string HIGHSCORE_FILE_PATH = Path.Combine(Application.persistentDataPath, "Highscores.json");
    public static string APP_SETTINGS_PATH = "Settings/AppSettings";
    public static string UI_ROOT_PATH = "Prefabs/UI";
    public static string DIALOGS_ROOT_PATH = "Prefabs/UI/Dialogs";
    public static string PROJECTILE_PREFAB_PATH = "Prefabs/ProjectileView";
    public static string ROCKETS_PREFAB_PATH = "Prefabs/RocketView";
    public static string COIN_PREFAB_PATH = "Prefabs/CoinView";
    public static string ENEMYSHIP_PREFAB_PATH = "Prefabs/EnemyShip";
    public static string PLAYERSHIP_PREFAB_PATH = "Prefabs/PlayerShip";
    public static string DIALOGS_PARENT_NAME = "[Dialogs]";
    public static string PROJECTILE_POOL_PARENT_NAME = "[ProjectilePoolParent]";
    public static string ROCKETS_POOL_PARENT_NAME = "[RocketsPoolParent]";
    public static string COIN_POOL_PARENT_NAME = "[CoinPoolParent]";
    public static string SHIP_POOL_PARENT_NAME = "[ShipPoolParent]";
    public static string COROUTINE_HELPER_NAME = "[CoroutineHelper]";
    public static WaitForSeconds WAIT_FOR_ONE_SECOND_DELAY = new WaitForSeconds(1f);
    public static string PRICE_STRING_TEMPLATE = "{0} <sprite=0>";
    public static int PLAYER_SHIP_ID = 0;
    public static Color GIZMOS_SHIP_SCAN_RADIUS_COLOR = new Color(Color.red.r, 0f, 0f, 0.25f);
    public static Color GIZMOS_PROJECTILE_EXPLOSION_COLOR = new Color(Color.red.r, 0f, 0f, 0.25f);
    public static float GIZMOS_TARGET_POSITION_SPHERE_RADIUS = 0.2f;
    public static Color GIZMOS_TARGET_POSITION_SPHERE_COLOR = Color.blue;
    public static int SHIPS_LAYER_MASK = 1 << LayerMask.NameToLayer("Ships");
    public static int HIGHSCORE_NAME_MAX_LENGTH = 12;
    public static WaitForSeconds SHIPS_SCAN_INTERVAL = new WaitForSeconds(0.1f);
}
