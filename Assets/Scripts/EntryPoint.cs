﻿using UnityEngine;

public class EntryPoint : MonoBehaviour
{
    private void Awake()
    {
        App.AppService.NavigationService.OpenMainMenuDialog();
    }
}
